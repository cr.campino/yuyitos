from django.contrib import admin
from .models import *
admin.site.register(Abono)
admin.site.register(Cliente)
admin.site.register(Credito)
admin.site.register(Empresa)
admin.site.register(FamiliaProducto)
#admin.site.register(IngresoProducto)
admin.site.register(OrdenDePedido)
admin.site.register(OrdenProductos)
admin.site.register(Producto)
admin.site.register(Proveedor)
admin.site.register(Recepcion)
admin.site.register(TipoProducto)
admin.site.register(TipoVenta)
admin.site.register(Venta)
admin.site.register(VentaProducto)
# Register your models here.
admin.site.site_header = "Sitio web de inventarios"
admin.site.site_title = "Portal de inventarios"
admin.site.index_title = "Bienvenido al portal de administración"