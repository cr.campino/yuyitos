from logging import exception
import json
from dateutil.parser import *
from json import JSONEncoder
import os
from django.conf import settings
from django.template import Context
from django.template.loader import get_template
from xhtml2pdf import pisa
from django.db import transaction
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.forms.formsets import BaseFormSet, formset_factory
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.middleware.csrf import logger
from django.shortcuts import get_object_or_404, redirect, render
from django.template.context_processors import csrf
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView, ListView, TemplateView, View
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from datetime import datetime, date, timedelta
from django.db.models.functions import Coalesce
from django.db.models import Sum, F
from io import BytesIO
from django import http
from json import dumps
from .mixins import ValidatePermissionRequiredMixin
from .forms import (ClienteForm, FamiliaForm, ReportCompraForm, AbonoForm, RecepcionForm, 
                    ProductoForm, ProveedorForm, TestForm, TipoForm, VentaForm, TipoVForm, ReportForm, CompraForm)
from .models import (Cliente, FamiliaProducto, OrdenDePedido, OrdenProductos, Abono, Recepcion, IngresoProducto,
                     Producto, Proveedor, TipoProducto, Venta, VentaProducto, TipoVenta,Empresa, Credito)

class IndexView(TemplateView):
    template_name = 'index.html'

class DashboardView(TemplateView):
    template_name = 'views/inicio/inicio.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'get_graph_sales_year_month':
                data = {
                    'name': 'Totales de venta',
                    'showInLegend': False,
                    'colorByPoint': True,
                    'data': self.get_graph_sales_year_month()
                }
            elif action == 'get_graph_sales_products_year_month':
                data = {
                    'name': 'Totales',
                    'colorByPoint': True,
                    'data': self.get_graph_sales_products_year_month(),
                }
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_graph_sales_year_month(self):
        data = []
        try:
            year = datetime.now().year
            for m in range(1, 13):
                total = Venta.objects.filter(fecha_hora__year=year, fecha_hora__month=m).aggregate(
                    r=Coalesce(Sum('total'), 0)).get('r')
                data.append(int(total))
        except:
            pass
        return data

    def get_graph_sales_products_year_month(self):
        data = []
        year = datetime.now().year
        month = datetime.now().month
        try:
            for p in Producto.objects.all():
                total = VentaProducto.objects.filter(venta__fecha_hora__year=year, venta__fecha_hora__month=month,
                                               codigo_id=p.codigo).aggregate(
                    r=Coalesce(Sum('subtotal'), 0)).get('r')
                if total > 0:
                    data.append({
                        'name': p.descripcion,
                        'y': int(total)
                    })
        except:
            pass
        return data

    def get_context_data(self, **kwargs):
        now = datetime.now()
        
        cant = OrdenDePedido.objects.all()
        compra = cant.filter(fecha_orden__month=format(now.month)).count()
        ven = Venta.objects.all()
        venta = ven.filter(fecha_hora__day=format(now.day)).count()
        prod = Producto.objects.all()
        producto = prod.filter(fecha_vencimiento__isnull=True).count()
        totalprod = Producto.objects.count()
        perecederos = totalprod - producto
        vence = prod.filter(fecha_vencimiento__lte=date.today()+timedelta(days=30)).count()
        empresa = Empresa.objects.filter(id_empresa=1).first()
        credi = Credito.objects.filter(estado_credito='No Pagado').count()
        context = super().get_context_data(**kwargs)
        context['panel'] = 'Panel de administrador'
        context['graph_sales_year_month'] = self.get_graph_sales_year_month()
        context['dash_proveedor'] = Proveedor.objects.count()
        context['dash_cliente'] = Cliente.objects.count()
        context['dash_prod'] = Producto.objects.count()
        context['dash_compra'] = compra
        context['dash_venta'] = venta
        context['dash_pere'] = perecederos
        context['dash_vence'] = vence
        context['empresa'] = empresa
        context['dash_credito'] = credi
        return context

class ProductoListView(LoginRequiredMixin, ListView):
    model = Producto
    context_object_name = 'producto'
    template_name = 'views/producto/producto_list.html'

    @method_decorator(permission_required('almacen.view_producto',login_url='/productos/'))
    def dispatch(self,request, *args, **kwargs):
        return super(ProductoListView, self).dispatch(request, *args, **kwargs)

            
    def get_context_data(self, **kwargs):
        empresa = Empresa.objects.filter(id_empresa=1).first()
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Productos'
        context['create_url'] = reverse_lazy('almacen:producto_create')
        context['list_url'] = reverse_lazy('almacen:producto_list')
        context['entity'] = 'Productos'
        context['empresa'] = empresa
        return context
            
class ProductoDetailView(LoginRequiredMixin, DetailView):
    model = Producto
    context_object_name = 'producto'
    template_name = 'views/producto/producto_detail.html'
    @method_decorator(permission_required('almacen.view_producto',reverse_lazy('almacen:producto_list')))
    def dispatch(self, request, *args, **kwargs):
        return super(ProductoDetailView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        empresa = Empresa.objects.filter(id_empresa=1).first()
        context = super().get_context_data(**kwargs)
        context['title'] = 'Detalle Producto'
        context['list_url'] = reverse_lazy('almacen:producto_list')
        context['empresa'] = empresa
        return context

class ProductoCreate(LoginRequiredMixin, CreateView):
    model = Producto
    form_class = ProductoForm
    success_url = reverse_lazy('almacen:producto_list')
    template_name = 'views/producto/producto_create.html'
    url_redirect = success_url

    @method_decorator(permission_required('almacen.add_producto',reverse_lazy('almacen:producto_list')))
    def dispatch(self, request, *args, **kwargs):
        return super(ProductoCreate, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
                messages.success(request,'Producto creado correctamente.')
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['boton'] = 'Agregar'
        context['list_url'] = reverse_lazy('almacen:producto_list')
        context['action'] = 'add'
        return context

class ProductoUpdate(LoginRequiredMixin, UpdateView):
    model = Producto
    form_class = ProductoForm
    template_name = 'views/producto/producto_create.html'
    success_url = reverse_lazy('almacen:producto_list')

    @method_decorator(permission_required('almacen.change_producto',reverse_lazy('almacen:producto_list')))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
                messages.success(request,'Producto modificado correctamente.')
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['boton'] = 'Modificar'
        context['list_url'] = self.success_url
        context['action'] = 'edit'
        return context

class ProductoDelete(LoginRequiredMixin, DeleteView):
    model = Producto
    success_url = reverse_lazy('almacen:producto_list')
    template_name = 'views/producto/producto_delete.html'

    @method_decorator(permission_required('almacen.delete_producto',reverse_lazy('almacen:producto_list')))
    def dispatch(self, request, *args, **kwargs):
        return super(ProductoDelete, self).dispatch(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['parrafo'] = '¿Desea eliminar el producto'
        context['list_url'] = reverse_lazy('almacen:producto_list')
        return context

class FamiliaListView(LoginRequiredMixin, ListView):
    model = FamiliaProducto
    context_object_name = 'familia'
    template_name = 'views/familia/familia_list.html'
    @method_decorator(permission_required('almacen.view_familiaproducto',login_url='/familia/'))
    def dispatch(self, *args, **kwargs):
        return super(FamiliaListView, self).dispatch(*args, **kwargs)
            
class FamiliaDetailView(LoginRequiredMixin, DetailView):
    model = FamiliaProducto
    context_object_name = 'familia'
    template_name = 'views/familia/familia_detail.html'
    @method_decorator(permission_required('almacen.view_familiaproducto',reverse_lazy('almacen:familia_list')))
    def dispatch(self, *args, **kwargs):
        return super(FamiliaDetailView, self).dispatch(*args, **kwargs)

class FamiliaCreate(LoginRequiredMixin, CreateView):
    model = FamiliaProducto
    form_class = FamiliaForm
    success_url = reverse_lazy('almacen:familia_list')
    template_name = 'views/familia/familia_create.html'

    @method_decorator(permission_required('almacen.add_familiaproducto',reverse_lazy('almacen:familia_list')))
    def dispatch(self,request, *args, **kwargs):
        return super(FamiliaCreate, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request,'Familia de Producto creado correctamente.')
            return HttpResponseRedirect(self.success_url)
        self.object = None
        context = self.get_context_data(**kwargs)
        context['form'] = form
        return render(request, self.template_name, context)

        
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['boton'] = 'Agregar'
        context['list_url'] = self.success_url
        return context

class FamiliaUpdate(LoginRequiredMixin, UpdateView):
    model = FamiliaProducto
    form_class = FamiliaForm
    template_name = 'views/familia/familia_update.html'
    success_url = reverse_lazy('almacen:familia_list')

    @method_decorator(permission_required('almacen.change_familiaproducto',reverse_lazy('almacen:familia_list')))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
                messages.success(request,'Familia de Producto modificado correctamente.')
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['boton'] = 'Modificar'
        context['list_url'] = self.success_url
        context['action'] = 'edit'
        return context

class FamiliaDelete(LoginRequiredMixin, DeleteView):
    model = FamiliaProducto
    success_url = reverse_lazy('almacen:familia_list')
    template_name = 'views/familia/familia_delete.html'

    @method_decorator(permission_required('almacen.delete_familiaproducto',reverse_lazy('almacen:familia_list')))
    def dispatch(self, *args, **kwargs):
        return super(FamiliaDelete, self).dispatch(*args, **kwargs)

class TipoListView(LoginRequiredMixin, ListView):
    model = TipoProducto
    context_object_name = 'tipo'
    template_name = 'views/tipoproduct/tipo_list.html'
    
    @method_decorator(permission_required('almacen.view_tipoproducto',login_url='/tipoproducto/'))
    def dispatch(self, *args, **kwargs):
        return super(TipoListView, self).dispatch(*args, **kwargs)
            
class TipoDetailView(LoginRequiredMixin, DetailView):
    model = TipoProducto
    context_object_name = 'tipo'
    template_name = 'views/tipoproduct/tipo_detail.html'
    @method_decorator(permission_required('almacen.view_tipoproducto',reverse_lazy('almacen:tipo_list')))
    def dispatch(self, *args, **kwargs):
        return super(TipoDetailView, self).dispatch(*args, **kwargs)

class TipoCreate(LoginRequiredMixin, CreateView):
    model = TipoProducto
    form_class = TipoForm
    success_url = reverse_lazy('almacen:tipo_list')
    template_name = 'views/tipoproduct/tipo_create.html'

    @method_decorator(permission_required('almacen.add_tipoproducto',reverse_lazy('almacen:tipo_list')))
    def dispatch(self, request, *args, **kwargs):
        return super(TipoCreate, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request,'Tipo de Producto creado correctamente.')
            return HttpResponseRedirect(self.success_url)
        self.object = None
        context = self.get_context_data(**kwargs)
        context['form'] = form
        return render(request, self.template_name, context)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['boton'] = 'Agregar'
        context['list_url'] = self.success_url
        return context


class TipoUpdate(LoginRequiredMixin, UpdateView):
    model = TipoProducto
    form_class = TipoForm
    template_name = 'views/tipoproduct/tipo_update.html'
    success_url = reverse_lazy('almacen:tipo_list')

    @method_decorator(permission_required('almacen.change_tipoproducto',reverse_lazy('almacen:tipo_list')))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
                messages.success(request,'Tipo de producto modificado correctamente.')
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['boton'] = 'Modificar'
        context['list_url'] = self.success_url
        context['action'] = 'edit'
        return context

class TipoDelete(LoginRequiredMixin, DeleteView):
    model = TipoProducto
    success_url = reverse_lazy('almacen:tipo_list')
    template_name = 'views/tipoproduct/tipo_delete.html'

    @method_decorator(permission_required('almacen.delete_tipoproducto',reverse_lazy('almacen:tipo_list')))
    def dispatch(self, *args, **kwargs):
        return super(TipoDelete, self).dispatch(*args, **kwargs)

class TipoVListView(LoginRequiredMixin, ListView):
    model = TipoVenta
    context_object_name = 'tipo'
    template_name = 'views/tipoventa/tipo_list.html'
    
    @method_decorator(permission_required('almacen.view_tipoventa',login_url='/tipoventa/'))
    def dispatch(self, *args, **kwargs):
        return super(TipoVListView, self).dispatch(*args, **kwargs)

class TipoVCreate(LoginRequiredMixin, CreateView):
    model = TipoVenta
    form_class = TipoVForm
    success_url = reverse_lazy('almacen:tipov_list')
    template_name = 'views/tipoventa/tipo_create.html'

    @method_decorator(permission_required('almacen.add_tipoventa',reverse_lazy('almacen:tipov_list')))
    def dispatch(self, request, *args, **kwargs):
        return super(TipoVCreate, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request,'Tipo de Venta creado correctamente.')
            return HttpResponseRedirect(self.success_url)
        self.object = None
        context = self.get_context_data(**kwargs)
        context['form'] = form
        return render(request, self.template_name, context)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['list_url'] = self.success_url
        return context

class ClienteListView(LoginRequiredMixin, ListView):
    model = Cliente
    context_object_name = 'cliente'
    template_name = 'views/cliente/cliente_list.html'
    
    @method_decorator(permission_required('almacen.view_cliente',login_url='/clientes/'))
    def dispatch(self, *args, **kwargs):
        return super(ClienteListView, self).dispatch(*args, **kwargs)

class ClienteDetailView(LoginRequiredMixin, DetailView):
    model = Cliente
    context_object_name = 'cliente'
    template_name = 'views/cliente/cliente_detail.html'
    @method_decorator(permission_required('almacen.view_cliente',reverse_lazy('almacen:cliente_list')))
    def dispatch(self, *args, **kwargs):
        return super(ClienteDetailView, self).dispatch(*args, **kwargs)

class ClienteCreate(LoginRequiredMixin, CreateView):
    model = Cliente
    form_class = ClienteForm
    success_url = reverse_lazy('almacen:cliente_list')
    template_name = 'views/cliente/cliente_create.html'

    @method_decorator(permission_required('almacen.add_cliente',reverse_lazy('almacen:cliente_list')))
    def dispatch(self, request, *args, **kwargs):
        return super(ClienteCreate, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request,'Cliente creado correctamente.')
            return HttpResponseRedirect(self.success_url)
        self.object = None
        context = self.get_context_data(**kwargs)
        context['form'] = form
        return render(request, self.template_name, context)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['boton'] = 'Agregar'
        context['list_url'] = self.success_url
        return context


class ClienteUpdate(LoginRequiredMixin, UpdateView):
    model = Cliente
    form_class = ClienteForm
    template_name = 'views/cliente/cliente_update.html'
    success_url = reverse_lazy('almacen:cliente_list')

    @method_decorator(permission_required('almacen.change_cliente',reverse_lazy('almacen:cliente_list')))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
                messages.success(request,'Cliente modificado correctamente.')
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['boton'] = 'Modificar'
        context['list_url'] = self.success_url
        context['action'] = 'edit'
        return context

class ClienteDelete(LoginRequiredMixin, DeleteView):
    model = Cliente
    success_url = reverse_lazy('almacen:cliente_list')
    template_name = 'views/cliente/cliente_delete.html'

    @method_decorator(permission_required('almacen.delete_cliente',reverse_lazy('almacen:cliente_list')))
    def dispatch(self, *args, **kwargs):
        return super(ClienteDelete, self).dispatch(*args, **kwargs)

class ProveedorListView(LoginRequiredMixin, ListView):
    model = Proveedor
    context_object_name = 'proveedor'
    template_name = 'views/proveedor/proveedor_list.html'
    
    @method_decorator(permission_required('almacen.view_proveedor',login_url='/proveedor/'))
    def dispatch(self, *args, **kwargs):
        return super(ProveedorListView, self).dispatch(*args, **kwargs)

class ProveedorDetailView(LoginRequiredMixin, DetailView):
    model = Proveedor
    context_object_name = 'proveedor'
    template_name = 'views/proveedor/proveedor_detail.html'
    @method_decorator(permission_required('almacen.view_proveedor',reverse_lazy('almacen:proveedor_list')))
    def dispatch(self, *args, **kwargs):
        return super(ProveedorDetailView, self).dispatch(*args, **kwargs)

class ProveedorCreate(LoginRequiredMixin, CreateView):
    model = Proveedor
    form_class = ProveedorForm
    success_url = reverse_lazy('almacen:proveedor_list')
    template_name = 'views/proveedor/proveedor_create.html'

    @method_decorator(permission_required('almacen.add_proveedor',reverse_lazy('almacen:proveedor_list')))
    def dispatch(self, request, *args, **kwargs):
        return super(ProveedorCreate, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request,'Proveedor creado correctamente.')
            return HttpResponseRedirect(self.success_url)
        self.object = None
        context = self.get_context_data(**kwargs)
        context['form'] = form
        return render(request, self.template_name, context)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['boton'] = 'Agregar'
        context['list_url'] = self.success_url
        return context


class ProveedorUpdate(LoginRequiredMixin, UpdateView):
    model = Proveedor
    form_class = ProveedorForm
    template_name = 'views/proveedor/proveedor_update.html'
    success_url = reverse_lazy('almacen:proveedor_list')

    @method_decorator(permission_required('almacen.change_proveedor',reverse_lazy('almacen:proveedor_list')))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
                messages.success(request,'Proveedor modificado correctamente.')
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['boton'] = 'Modificar'
        context['list_url'] = self.success_url
        context['action'] = 'edit'
        return context

class ProveedorDelete(LoginRequiredMixin, DeleteView):
    model = Proveedor
    success_url = reverse_lazy('almacen:proveedor_list')
    template_name = 'views/proveedor/proveedor_delete.html'

    @method_decorator(permission_required('almacen.delete_proveedor',reverse_lazy('almacen:proveedor_list')))
    def dispatch(self, *args, **kwargs):
        return super(ProveedorDelete, self).dispatch(*args, **kwargs)

class OrdenListView(LoginRequiredMixin, ListView):
    model = OrdenDePedido
    template_name = 'views/compra/compra_list.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(OrdenListView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdatacompra':
                data = []
                for i in OrdenDePedido.objects.filter(estado_orden__icontains='Pendiente'):
                    data.append(i.toJSON())
            elif action == 'searchdatacomprafin':
                data = []
                for i in OrdenDePedido.objects.filter(estado_orden__icontains='Finalizado'):
                    data.append(i.toJSON())
            elif action == 'search_details_prod_compra':
                data = []
                for i in OrdenProductos.objects.filter(orden_id=request.POST['id']):
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Órdenes de Compra'
        context['create_url'] = reverse_lazy('almacen:compra_create')
        context['list_url'] = reverse_lazy('almacen:compra_list')
        context['entity'] = 'Compras'
        return context

class OrdenCreate(LoginRequiredMixin, CreateView):
    model = OrdenDePedido
    form_class = CompraForm
    success_url = reverse_lazy('almacen:compra_list')
    template_name = 'views/compra/compra_create.html'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    @method_decorator(permission_required('almacen.add_ordendepedido',reverse_lazy('almacen:compra_list')))
    def dispatch(self, request, *args, **kwargs):
            return super(OrdenCreate, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']

            if action == 'search_products':
                data = [{'id':'','text':'...'}]
                prods = Producto.objects.filter(id_proveedor_id=request.POST['id'])[0:10]
                for i in prods:
                    item = i.toJSON()
                    item['id'] = i.codigo
                    item['text'] = i.descripcion
                    item['data'] = i.id_proveedor.toJSON()
                    data.append(item)
            elif action == 'add':
                with transaction.atomic():
                    orden = json.loads(request.POST['orden'])
                    compra = OrdenDePedido()
                    compra.fecha_orden = orden['fecha_orden']
                    compra.id_proveedor_id = orden['id_proveedor']
                    compra.estado_orden = 'Pendiente'
                    compra.total = int(orden['total'])
                    compra.save()
                    for i in orden['products']:
                        det = OrdenProductos()
                        det.orden_id = compra.id
                        det.codigo_id = i['codigo']
                        det.cantidad_orden_producto = int(i['cant'])
                        det.precio = int(i['precio_compra'])
                        det.subtotal = int(i['subtotal'])
                        det.save()
                    data = {'id': compra.id}
                messages.success(request,'Orden de Compra creada correctamente.')
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de una Orden de Compra'
        context['entity'] = 'Compras'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['det'] = []
        return context


class DateTimeEncoder(JSONEncoder):
        #Override the default method
    def default(self, obj):
        if isinstance(obj, (date)):
            return obj.isoformat()

class OrdenUpdate(LoginRequiredMixin, UpdateView):
    model = OrdenDePedido
    form_class = CompraForm
    success_url = reverse_lazy('almacen:compra_list')
    template_name = 'views/compra/compra_create.html'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    @method_decorator(permission_required('almacen.change_ordendepedido',reverse_lazy('almacen:compra_list')))
    def dispatch(self, request, *args, **kwargs):
            return super(OrdenUpdate, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']

            if action == 'search_products':
                data = [{'id':'','text':'...'}]
                prods = Producto.objects.filter(id_proveedor_id=request.POST['id'])[0:10]
                for i in prods:
                    item = i.toJSON()
                    item['id'] = i.codigo
                    item['text'] = i.descripcion
                    item['data'] = i.id_proveedor.toJSON()
                    data.append(item)
            elif action == 'edit':
                with transaction.atomic():
                    orden = json.loads(request.POST['id'])
                    compra = self.get_object()
                    compra.fecha_orden = orden['fecha_orden']
                    compra.id_proveedor_id = orden['id_proveedor']
                    compra.estado_orden = 'Pendiente'
                    compra.total = int(orden['total'])
                    compra.save()
                    compra.ordenproductos_set.all().delete()
                    for i in orden['products']:
                        det = OrdenProductos()
                        det.orden_id = compra.id
                        det.codigo_id = i['codigo']
                        det.cantidad_orden_producto = int(i['cant'])
                        det.precio = int(i['precio_compra'])
                        det.subtotal = int(i['subtotal'])
                        det.save()
                    data = {'id': compra.id}
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_details_product(self):
        data = []
        try:
            for i in OrdenProductos.objects.filter(orden_id=self.get_object().id):
                item = i.codigo.toJSON()
                item['cantidad_orden_producto'] = i.cantidad_orden_producto
                data.append(item)
        except:
            pass
        return data

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de una Orden de Compra'
        context['entity'] = 'Compras'
        context['list_url'] = self.success_url
        context['action'] = 'edit'
        context['det'] = json.dumps(self.get_details_product(), cls=DateTimeEncoder)
        return context

class OrdenDelete(LoginRequiredMixin, DeleteView):
    model = OrdenDePedido
    success_url = reverse_lazy('almacen:compra_list')
    template_name = 'views/compra/compra_delete.html'
    url_redirect = success_url

    @method_decorator(permission_required('almacen.delete_ordendepedido',reverse_lazy('almacen:compra_list')))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['parrafo'] = '¿Desea eliminar la Orden de Compra'
        context['list_url'] = self.success_url
        return context

class TestView(TemplateView):
    template_name = 'views/compra/tests.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            
            if action == 'search_product_id':
                data = [{'id':'','text':'---------'}] 
                for i in Producto.objects.filter(id_proveedor_id=request.POST['id']):
                    data.append({'id': i.codigo, 'text':i.descripcion, 'data': i.id_proveedor.toJSON()})
            elif action == 'autocomplete':
                data = []
                for i in Proveedor.objects.filter(nombre_proveedor__icontains=request.POST['term'])[0:10]:
                    item = i.toJSON()
                    item['text'] = i.nombre_proveedor
                    data.append(item)
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Select Aninados | Django'
        context['form'] = TestForm()
        return context

class VentaListView(LoginRequiredMixin, ListView):
    model = Venta
    template_name = 'views/venta/venta_list.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(VentaListView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Venta.objects.all():
                    data.append(i.toJSON())
            elif action == 'search_details_prod':
                data = []
                for i in VentaProducto.objects.filter(venta_id=request.POST['id']):
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    
    def get_context_data(self, **kwargs):
        empresa = Empresa.objects.filter(id_empresa=1).first()
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Ventas'
        context['create_url'] = reverse_lazy('almacen:venta_create')
        context['list_url'] = reverse_lazy('almacen:venta_list')
        context['entity'] = 'Ventas'
        context['empresa'] = empresa
        return context

class VentaCreate(LoginRequiredMixin, CreateView):
    model = Venta
    form_class = VentaForm
    success_url = reverse_lazy('almacen:venta_list')
    template_name = 'views/venta/venta_create.html'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    @method_decorator(permission_required('almacen.add_venta',reverse_lazy('almacen:venta_list')))
    def dispatch(self, request, *args, **kwargs):
            return super(VentaCreate, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_products':
                data = []
                prods = Producto.objects.filter(descripcion__icontains=request.POST['term'])[0:10]
                for i in prods:
                    item = i.toJSON()
                    item['text'] = i.descripcion
                    data.append(item)
            elif action == 'add':
                with transaction.atomic():
                    vents = json.loads(request.POST['vents'])
                    sale = Venta()
                    sale.fecha_hora = vents['fecha_hora']
                    sale.id_tipo_venta_id = vents['id_tipo_venta']                
                    sale.id_cliente_id = vents['id_cliente']
                    sale.subtotal = int(vents['subtotal'])
                    sale.total = int(vents['total'])
                    sale.save()
                    
                    for i in vents['products']:
                        det = VentaProducto()
                        det.venta_id = sale.id
                        det.codigo_id = i['codigo']
                        det.cantidad_venta_producto = int(i['cant'])
                        det.precio = int(i['precio_venta'])
                        det.subtotal = int(i['subtotal'])
                        det.save()
                    data = {'id': sale.id}
                    if sale.id_tipo_venta_id == '1cre':
                        cre = Credito()
                        cre.venta_id = sale.id
                        cre.estado_credito = "No Pagado"
                        cre.fecha_convenida = date.today()+timedelta(days=30)
                        cre.monto_venta = sale.total
                        cre.monto_pendiente = sale.total
                        cre.id_cliente_id = sale.id_cliente_id
                        cre.save()
                messages.success(request,'Venta realizada correctamente.')   
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de una Venta'
        context['entity'] = 'Ventas'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['det'] = []
        return context

class VentaInvoicePdfView(View):
    def link_callback(self, uri, rel):
        """
        Convert HTML URIs to absolute system paths so xhtml2pdf can access those
        resources
        """
        # use short variable names
        sUrl = settings.STATIC_URL  # Typically /static/
        sRoot = settings.STATIC_ROOT  # Typically /home/userX/project_static/
        mUrl = settings.MEDIA_URL  # Typically /static/media/
        mRoot = settings.MEDIA_ROOT  # Typically /home/userX/project_static/media/

        # convert URIs to absolute system paths
        if uri.startswith(mUrl):
            path = os.path.join(mRoot, uri.replace(mUrl, ""))
        elif uri.startswith(sUrl):
            path = os.path.join(sRoot, uri.replace(sUrl, ""))
        else:
            return uri  # handle absolute uri (ie: http://some.tld/foo.png)

        # make sure that file exists
        if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
        return path
        
    def get(self, request, *args, **kwargs):
        try:
            template = get_template('views/venta/invoice.html')
            context = {
                'sale': Venta.objects.get(pk=self.kwargs['pk']),
                'comp': {'name': 'YUYITOS S.A.', 'ruc': '9999999999999', 'address': 'Calle Nueva, Santiago, Chile'},
                'icon': '{}{}'.format(settings.MEDIA_URL, 'logo.png')
            }
            html = template.render(context)
            response = HttpResponse(content_type='application/pdf')
            # response['Content-Disposition'] = 'attachment; filename="report.pdf"'
            pisaStatus = pisa.CreatePDF(
                html, dest=response,
                link_callback=self.link_callback
            )
            return response
        except:
            pass
        return HttpResponseRedirect(reverse_lazy('almacen:venta_list'))

class BarCode(TemplateView):      
    def get(self, request, *args, **kwargs):
        template = get_template('views/producto/barcode.html')
        context = {
            'prod': Producto.objects.all(),
        }
        html = template.render(context) 
        result = BytesIO()
        pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), result)
        if not pdf.err:
            return http.HttpResponse(result.getvalue(), content_type="application/pdf")
        return http.HttpResponse('Ocurrio un error al genera el reporte %s' % cgi.escape(html))

class ReportVentaView(TemplateView):
    template_name = 'views/venta/report.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_report':
                data = []
                start_date = request.POST.get('start_date', '')
                end_date = request.POST.get('end_date', '')
                search = Venta.objects.all()
                if len(start_date) and len(end_date):
                    search = search.filter(fecha_hora__range=[start_date, end_date])
                s = 0
                for s in search:
                    if s.id_cliente is not None:
                        data.append([
                            s.id,
                            s.id_tipo_venta.tipo_venta,
                            s.id_cliente.nombre_cliente,
                            s.fecha_hora.strftime('%d/%m/%Y'),
                            format(s.subtotal, '.0f'),
                            format(s.total, '.0f'),
                        ])
                    else:
                         data.append([
                            s.id,
                            s.id_tipo_venta.tipo_venta,
                            "",
                            s.fecha_hora.strftime('%d/%m/%Y'),
                            format(s.subtotal, '.0f'),
                            format(s.total, '.0f'),
                        ])

                subtotal = search.aggregate(r=Coalesce(Sum('subtotal'), 0)).get('r')
                total = search.aggregate(r=Coalesce(Sum('total'), 0)).get('r')

                data.append([
                    '---',
                    '---',
                    '---',
                    '---',
                    format(subtotal, '.0f'),
                    format(total, '.0f'),
                ])
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Reporte de Ventas'
        context['entity'] = 'Reportes'
        context['list_url'] = reverse_lazy('almacen:venta_reporte')
        context['form'] = ReportForm()
        return context


def write_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html = template.render(context_dict)

    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), result)
    if not pdf.err:
        return http.HttpResponse(result.getvalue(), content_type="application/pdf")
    return http.HttpResponse('Ocurrio un error al genera el reporte %s' % cgi.escape(html))
        
def generar_reporte_productos(request):
    template = 'views/producto/report.html'
    productos = Producto.objects.all()

    total_precio_compra = 0
    for expe in productos:
        total_precio_compra = (expe.stock * expe.precio_compra) + total_precio_compra

    total_precio_venta = 0
    for expe in productos:
        total_precio_venta = (expe.stock * expe.precio_venta) + total_precio_venta

    ganancia = total_precio_venta - total_precio_compra
    context = {
        'pagesize': 'legal',
        'productos': productos,
        'total_precio_compra': total_precio_compra,
        'total_precio_venta': total_precio_venta,
        'ganancia': ganancia,
        'comp': {'name': 'YUYITOS S.A.', 'title': 'Reporte Productos'},
        'icon': '{}{}'.format(settings.MEDIA_URL, 'logo.png')
    }
           
    return write_pdf(template, context)

class ReportCompraView(TemplateView):
    template_name = 'views/compra/report.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_report':
                data = []
                start_date = request.POST.get('start_date', '')
                end_date = request.POST.get('end_date', '')
                search = OrdenDePedido.objects.all()
                if len(start_date) and len(end_date):
                    search = search.filter(fecha_orden__range=[start_date, end_date])
                s = 0
                for s in search:
                    data.append([
                        s.id,
                        s.id_proveedor.nombre_proveedor,
                        s.fecha_orden.strftime('%d/%m/%Y'),
                        s.estado_orden,
                        format(s.total, '.0f'),
                    ])

                total = search.aggregate(r=Coalesce(Sum('total'), 0)).get('r')

                data.append([
                    '---',
                    '---',
                    '---',
                    '---',
                    format(total, '.0f'),
                ])
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Reporte de Compras'
        context['entity'] = 'Reportes'
        context['list_url'] = reverse_lazy('almacen:compra_reporte')
        context['form'] = ReportCompraForm()
        return context

#def realizarCompra(request):
    #return render(request, 'views/pages/realizarCompra.html')
class RecepcionView(LoginRequiredMixin, ListView):
    model = OrdenDePedido
    template_name = 'views/pages/recepcion.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(RecepcionView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdatacompra':
                data = []
                for i in OrdenDePedido.objects.filter(estado_orden='Pendiente'):
                    data.append(i.toJSON())
            elif action == 'search_details_prod_compra':
                data = []
                for i in OrdenProductos.objects.filter(orden_id=request.POST['id']):
                    data.append(i.toJSON())
            elif action == 'edit':
                data = []
                with transaction.atomic():
                    rep = Recepcion()
                    rep.orden_id = request.POST['id']
                    rep.fecha_recepcion = date.today()
                    rep.save()
                messages.success(request,'Recepción de compra realizada correctamente.') 
            elif action == 'search_details_rec':
                data = []
                for i in Recepcion.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Órdenes de Compra'
        context['form'] = RecepcionForm
        context['det'] = []
        return context

#def realizarventa(request):
    #return render(request, 'views/pages/realizarVenta.html')

#def consultarventa(request):
    #return render(request, 'views/pages/consultarVenta.html')

class CreditoListView(LoginRequiredMixin, ListView):
    model = Credito
    template_name = 'views/pages/credito.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Credito.objects.filter(estado_credito='No Pagado'):
                    data.append(i.toJSON())
            elif action == 'searchdatafinal':
                data = []
                for i in Credito.objects.filter(estado_credito='Pagado'):
                    data.append(i.toJSON())
            elif action == 'search_details_prod':
                data = []
                for i in Abono.objects.filter(credito_id=request.POST['id']):
                    data.append(i.toJSON())
            elif action == 'add':
                cre = Credito.objects.get(pk=request.POST['id'])
                abo = Abono()
                abo.abonado = request.POST['abonado']
                abo.credito_id = request.POST['id']
                abo.fecha_abono = date.today()
                abo.save()
                messages.success(request,'Pago a la venta al credito realizado correctamente.') 
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        empresa = Empresa.objects.filter(id_empresa=1).first()
        context = super().get_context_data(**kwargs)
        context['credito'] = Credito.objects.all()
        context['abono'] = Abono.objects.all()
        context['empresa'] = empresa
        context['form'] = AbonoForm()
        return context

#def credito(request):
    #return render(request, 'views/pages/credito.html')

#def consultarcompra(request):
    #return render(request, 'views/pages/consultarCompra.html')
