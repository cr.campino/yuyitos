from django.db import models

from django.conf import settings
from datetime import datetime, date, timedelta
from django.db.models import signals
from django.forms import model_to_dict
from django.urls import reverse 
from yuyitos.settings import MEDIA_URL, STATIC_URL

class Abono(models.Model):
    id = models.AutoField(primary_key=True, serialize=False)
    abonado = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    fecha_abono = models.DateField()
    credito = models.ForeignKey('Credito',  on_delete=models.CASCADE)

    def __str__(self):
        return self.abonado
    
    def toJSON(self):
        item = model_to_dict(self)
        item['abonado'] = format(self.abonado, '.0f')
        item['fecha_abono'] = self.fecha_abono.strftime('%d/%m/%Y')
        return item

    class Meta:
        managed = False
        db_table = 'abono'
    
"""
class BoletaImpresion(models.Model):
    nro_boleta = models.CharField(primary_key=True, max_length=30)
    id_venta = models.ForeignKey('Venta',  on_delete=models.CASCADE, db_column='id_venta')

    def __str__(self):
        return self.nro_boleta

    class Meta:
        managed = False
        db_table = 'boleta_impresion'
    
"""

class Cliente(models.Model):
    id_cliente = models.CharField(primary_key=True, max_length=30)
    nombre_cliente = models.CharField(max_length=60)
    direccion_cliente = models.CharField(max_length=60, blank=True, null=True)
    email_cliente = models.CharField(max_length=50, blank=True, null=True, unique=True)
    telefono_cliente = models.PositiveIntegerField()

    def toJSON(self):
        item = model_to_dict(self)
        return item

    def __str__(self):
        return self.nombre_cliente

    def get_absolute_url(self):
        return reverse('almacen:cliente_detail', args=[str(self.id_cliente)])

    class Meta:
        managed = False
        db_table = 'cliente'
    

class Credito(models.Model):
    id = models.AutoField(primary_key=True, serialize=False)
    estado_credito = models.CharField(max_length=30)
    fecha_convenida = models.DateField()
    monto_venta = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    monto_pendiente = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    venta = models.ForeignKey('Venta',  on_delete=models.CASCADE)
    id_cliente = models.ForeignKey(Cliente,  on_delete=models.CASCADE, db_column='id_cliente')

    def __str__(self):
        return self.id
    
    def toJSON(self):
        item = model_to_dict(self)
        item['fecha_convenida'] = self.fecha_convenida.strftime('%d/%m/%Y')
        item['monto_venta'] = format(self.monto_venta, '.0f')
        item['monto_pendiente'] = format(self.monto_pendiente, '.0f')
        item['venta'] = self.venta.toJSON()
        item['id_cliente'] = self.id_cliente.toJSON()
        item['det'] = [i.toJSON() for i in self.abono_set.all()]
        return item

    class Meta:
        managed = False
        db_table = 'credito'
    

class Empresa(models.Model):
    id_empresa = models.CharField(primary_key=True, max_length=30)
    nombre_empresa = models.CharField(max_length=30)
    simbolo_moneda = models.CharField(max_length=3)
    moneda_decimal = models.BooleanField()
    logo_empresa = models.ImageField(upload_to='photos')

    def __str__(self):
        return self.nombre_empresa
    
    def get_image(self):
        if self.logo_empresa:
            return '{}{}'.format(MEDIA_URL, self.logo_empresa)
        return '{}{}'.format(STATIC_URL, 'dist/img/empty.png')

    class Meta:
        managed = False
        db_table = 'empresa'


class FamiliaProducto(models.Model):
    id_familia = models.CharField(primary_key=True, max_length=30)
    familia = models.CharField(max_length=30, unique=True)

    def toJSON(self):
        item = model_to_dict(self)
        return item
    
    def __str__(self):
        return self.familia
        #return str(self.id_familia) + " | " + str(self.familia)

    def get_absolute_url(self):
        return reverse('almacen:familia-detail', args=[str(self.id_familia)])
    class Meta:
        managed = False
        db_table = 'familia_producto'

class IngresoProducto(models.Model):
    id = models.AutoField(primary_key=True, serialize=False)
    stock_ingreso = models.PositiveIntegerField()
    recepcion = models.ForeignKey('Recepcion',  on_delete=models.CASCADE)
    codigo = models.ForeignKey('Producto',  on_delete=models.CASCADE, db_column='codigo')

    def __str__(self):
        return self.id
    
    def toJSON(self):
        item = model_to_dict(self, exclude=['orden'])
        item['codigo'] = self.codigo.toJSON()
        return item

    class Meta:
        managed = False
        db_table = 'ingreso_producto'
    
"""
def update_stock(sender, instance, **kwargs):
    instance.producto.stock += instance.stock_ingreso
    instance.producto.save()

signals.post_save.connect(update_stock, sender=IngresoProducto, dispatch_uid="update_stock_count")
"""
class OrdenDePedido(models.Model):
    id = models.AutoField(primary_key=True, serialize=False)
    fecha_orden = models.DateField()
    estado_orden = models.CharField(max_length=30,default='Pendiente')
    id_proveedor = models.ForeignKey('Proveedor',  on_delete=models.CASCADE, db_column='id_proveedor')
    total = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    
    def __str__(self):
        return self.id

    def toJSON(self):
        item = model_to_dict(self)
        item['id_proveedor'] = self.id_proveedor.toJSON()
        item['estado_orden'] = self.estado_orden
        item['total'] = format(self.total, '.0f')
        item['fecha_orden'] = self.fecha_orden.strftime('%d/%m/%Y')
        item['det'] = [i.toJSON() for i in self.ordenproductos_set.all()]
        return item
    
    class Meta:
        managed = False
        db_table = 'orden_de_pedido'
    

class OrdenProductos(models.Model):    
    id = models.AutoField(primary_key=True)
    orden = models.ForeignKey(OrdenDePedido, on_delete=models.CASCADE)
    codigo = models.ForeignKey('Producto',  on_delete=models.CASCADE, db_column='codigo')
    cantidad_orden_producto = models.PositiveIntegerField()
    precio = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    subtotal = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    
    def __str__(self):
        return self.codigo.descripcion

    def toJSON(self):
        item = model_to_dict(self, exclude=['orden'])
        item['codigo'] = self.codigo.toJSON()
        item['precio'] = format(self.precio, '.0f')
        item['subtotal'] = format(self.subtotal, '.0f')
        return item

    class Meta:
        managed = False
        db_table = 'orden_productos'
    

class Producto(models.Model):
    codigo = models.CharField(primary_key=True, max_length=17)
    descripcion = models.CharField(max_length=80)
    precio_compra = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    precio_venta = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    stock = models.PositiveIntegerField()
    stock_critico = models.PositiveIntegerField()
    fecha_vencimiento = models.DateField(blank=True, null=False)
    image = models.ImageField(upload_to='product/%Y/%m/%d', null=True, blank=True, verbose_name='Imagen')
    id_proveedor = models.ForeignKey('Proveedor',  on_delete=models.CASCADE, db_column='id_proveedor')
    id_familia = models.ForeignKey(FamiliaProducto,  on_delete=models.CASCADE, db_column='id_familia')
    id_tipo_producto = models.ForeignKey('TipoProducto',  on_delete=models.CASCADE, db_column='id_tipo_producto')
    
    def get_date(self):
        hoy = date.today()
        dias = timedelta(days=30)
        hoy_mas_dias = hoy+dias
        return hoy_mas_dias
     
    def toJSON(self):
        item = model_to_dict(self)
        item['id_proveedor'] = self.id_proveedor.toJSON()
        item['id_familia'] = self.id_familia.toJSON()
        item['id_tipo_producto'] = self.id_tipo_producto.toJSON()
        item['image'] = self.get_image()
        item['precio_compra'] = format(self.precio_compra, '.0f')
        item['precio_venta'] = format(self.precio_venta, '.0f')
        return item

    def __str__(self):
        return self.descripcion

    def get_image(self):
        if self.image:
            return '{}{}'.format(MEDIA_URL, self.image)
        return '{}{}'.format(STATIC_URL, 'dist/img/empty.png')

    def get_absolute_url(self):
        return reverse('almacen:producto_detail', args=[str(self.codigo)])

    class Meta:
        managed = False
        db_table = 'producto'


class Proveedor(models.Model):
    id_proveedor = models.CharField(primary_key=True, max_length=30)
    nombre_proveedor = models.CharField(max_length=30)
    rubro_proveedor = models.CharField(max_length=60)
    email_proveedor = models.CharField(max_length=30, unique=True)
    telefono_proveedor = models.PositiveIntegerField()
    
    def __str__(self):
        return self.nombre_proveedor
    
    def toJSON(self):
        item = model_to_dict(self)
        return item

    def get_absolute_url(self):
        return reverse('almacen:proveedor_detail', args=[str(self.id_proveedor)])

    class Meta:
        managed = False
        db_table = 'proveedor'

class Recepcion(models.Model):
    id = models.AutoField(primary_key=True, serialize=False)
    fecha_recepcion = models.DateField()
    orden = models.ForeignKey(OrdenDePedido,  on_delete=models.CASCADE)

    def __str__(self):
        return self.id

    def toJSON(self):
        item = model_to_dict(self)
        item['fecha_recepcion'] = self.fecha_recepcion.strftime('%d/%m/%Y')
        item['det'] = [i.toJSON() for i in self.ingresoproducto_set.all()]
        return item

    class Meta:
        managed = False
        db_table = 'recepcion'


class TipoProducto(models.Model):
    id_tipo_producto = models.CharField(primary_key=True, max_length=30)
    tipo_producto = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return self.tipo_producto

    def toJSON(self):
        item = model_to_dict(self)
        return item
        
    def get_absolute_url(self):
        return reverse('almacen:tipo_detail', args=[str(self.id_tipo_producto)])

    class Meta:
        managed = False
        db_table = 'tipo_producto'


class TipoVenta(models.Model):
    id_tipo_venta = models.CharField(primary_key=True, max_length=30)
    tipo_venta = models.CharField(max_length=30)

    def __str__(self):
        return self.tipo_venta
    
    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        managed = False
        db_table = 'tipo_venta'


class Venta(models.Model):
    id = models.AutoField(primary_key=True, serialize=False)
    fecha_hora = models.DateField()
    subtotal = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    total = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    id_tipo_venta = models.ForeignKey(TipoVenta,  on_delete=models.CASCADE, db_column='id_tipo_venta')
    id_cliente = models.ForeignKey(Cliente,  on_delete=models.CASCADE, db_column='id_cliente', blank=True, null=True)

    def __str__(self):
        return self.id

    def toJSON(self):
        item = model_to_dict(self)
        if self.id_cliente:
            item['id_cliente'] = self.id_cliente.toJSON()    
        else:
            item['id_cliente'] = ""
        item['id_tipo_venta'] = self.id_tipo_venta.toJSON()
        item['subtotal'] = format(self.subtotal, '.0f')
        item['total'] = format(self.total, '.0f')
        item['fecha_hora'] = self.fecha_hora.strftime('%d/%m/%Y')
        item['det'] = [i.toJSON() for i in self.ventaproducto_set.all()]
        return item

    class Meta:
        managed = False
        db_table = 'venta'


class VentaProducto(models.Model):
    id = models.AutoField(primary_key=True, serialize=False)
    cantidad_venta_producto = models.IntegerField(default=0)
    codigo = models.ForeignKey(Producto,  on_delete=models.CASCADE, db_column='codigo')
    venta = models.ForeignKey(Venta,  on_delete=models.CASCADE, blank=True, null=True)
    precio = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    subtotal = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)

    def __str__(self):
        return self.codigo.descripcion

    def toJSON(self):
        item = model_to_dict(self, exclude=['venta'])
        item['codigo'] = self.codigo.toJSON()
        item['precio'] = format(self.precio, '.0f')
        item['subtotal'] = format(self.subtotal, '.0f')
        return item

    class Meta:
        managed = False
        db_table = 'venta_producto'

# Create your models here.
