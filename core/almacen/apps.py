from django.apps import AppConfig


class AlmacenConfig(AppConfig):
    name = 'core.almacen'
