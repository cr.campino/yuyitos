from django import forms
from .models import FamiliaProducto
from .models import TipoProducto
from .models import Proveedor
from .models import Cliente
from .models import Proveedor
from .models import Producto
from .models import OrdenDePedido
from .models import OrdenProductos
from .models import Venta
from .models import TipoVenta
from .models import Abono
from django.forms import ModelForm, inlineformset_factory
from django.forms import SelectDateWidget, ModelChoiceField, Select, Form, DateInput, TextInput, CharField,DateField
from django.utils import timezone
from datetime import datetime

from django.conf import settings

class ReportCompraForm(Form):
    date_range = CharField(widget=TextInput(attrs={
        'class': 'form-control',
        'autocomplete': 'off'
    }))

class ReportForm(Form):
    date_range = CharField(widget=TextInput(attrs={
        'class': 'form-control',
        'autocomplete': 'off'
    }))

class VentaForm(ModelForm):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        model = Venta
        fields = '__all__'
        widgets = {
            'fecha_hora': DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'fecha_hora',
                    'data-target': '#fecha_hora',
                    'data-toggle': 'datetimepicker'
                    
            }),
            'id_tipo_venta': Select(attrs={
                'class': 'form-control select2',
                'style': 'width: 100%',
                'data-placeholder': '...'
            }),
            'id_cliente': Select(attrs={
                'class': 'form-control select2',
                'style': 'width: 100%',
                'data-placeholder': '...',
                'disabled': 'disabled',
            }),
            'subtotal': TextInput(attrs={
                'readonly': True,
                'class': 'form-control',
            }),
            'total': TextInput(attrs={
                'readonly': True,
                'class': 'form-control',
            }),
        }

class CompraForm(ModelForm):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        model = OrdenDePedido
        fields = '__all__'
        widgets = {
            'fecha_orden': DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'fecha_orden',
                    'data-target': '#fecha_orden',
                    'data-toggle': 'datetimepicker'
                    
            }),
            'id_proveedor': Select(attrs={
                'class': 'form-control select2',
                'style': 'width: 100%',
                'data-placeholder': '...'
            }),
            'estado_orden': TextInput(attrs={
                'readonly': True,
                'class': 'form-control',
            }),
            'total': TextInput(attrs={
                'readonly': True,
                'class': 'form-control',
            })
        }
class TestForm(Form):
    proveedores = ModelChoiceField(queryset=Proveedor.objects.all(), widget=Select(attrs={
        'class': 'form-control select2',
        'style': 'width: 100%'
    }))

    products = ModelChoiceField(queryset=Producto.objects.none(), widget=Select(attrs={
        'class': 'form-control select2',
        'style': 'width: 100%'
    }))

    search = ModelChoiceField(queryset=Producto.objects.none(), widget=Select(attrs={
        'class': 'form-control select2',
        'style': 'width: 100%'
    }))

class RecepcionForm(ModelForm):    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        model = OrdenDePedido
        fields = ('id',)
        widgets = {
            'id': forms.NumberInput(attrs={
                'class': 'form-control',
                'type': 'hidden',
                'name': 'id',
                }),
        }
# ------------------------------------
class AbonoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['abonado'].widget.attrs['autofocus'] = True

    class Meta:
        model = Abono
        fields = ('abonado',)
        widgets = {
            'abonado': forms.NumberInput(attrs={
                'class': 'form-control',
                'autocomplete': 'off', 
                'type': 'number',
                'min': '1',
                'pattern': '^[0-9]+',
                'step':'1'
                }),
        }
    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

class FamiliaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['familia'].widget.attrs['autofocus'] = True

    class Meta:
        model = FamiliaProducto
        fields = '__all__'
        exclude = ('id_familia',)
        widgets = {
            'familia': forms.TextInput(attrs={'class': 'form-control','autocomplete': 'off'}),
        }
    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

class TipoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['tipo_producto'].widget.attrs['autofocus'] = True

    class Meta:
        model = TipoProducto
        fields = '__all__'
        exclude = ('id_tipo_producto',)
        widgets = {
            'tipo_producto': forms.TextInput(attrs={'class': 'form-control','autocomplete': 'off'}),
        }
    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

class TipoVForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['id_tipo_venta'].widget.attrs['autofocus'] = True

    class Meta:
        model = TipoVenta
        fields = '__all__'
        widgets = {
            'id_tipo_venta': forms.TextInput(attrs={'class': 'form-control','autocomplete': 'off'}),
            'tipo_venta': forms.TextInput(attrs={'class': 'form-control','autocomplete': 'off'}),
        }
    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:   
            data['error'] = str(e)
        return data

class ProveedorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['nombre_proveedor'].widget.attrs['autofocus'] = True

    class Meta:
        model = Proveedor
        fields = '__all__'
        exclude = ('id_proveedor',)
        widgets = {
            'nombre_proveedor': forms.TextInput(attrs={'class': 'form-control','autocomplete': 'off'}),
            'rubro_proveedor': forms.TextInput(attrs={'class': 'form-control','autocomplete': 'off'}),
            'email_proveedor': forms.EmailInput(attrs={'class': 'form-control','autocomplete': 'off','placeholder': 'example@example.com',}),
            'telefono_proveedor': forms.NumberInput(attrs={'class': 'form-control','autocomplete': 'off','placeholder': 'ej: 56950123456',}),
        }
    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

class ClienteForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['nombre_cliente'].widget.attrs['autofocus'] = True

    class Meta:
        model = Cliente
        fields = '__all__'
        exclude = ('id_cliente',)
        widgets = {
            'nombre_cliente': forms.TextInput(attrs={'class': 'form-control','autocomplete': 'off'}),
            'direccion_cliente': forms.TextInput(attrs={'class': 'form-control','autocomplete': 'off'}),
            'email_cliente': forms.EmailInput(attrs={'class': 'form-control','autocomplete': 'off','placeholder': 'example@example.com',}),
            'telefono_cliente': forms.NumberInput(attrs={'class': 'form-control','autocomplete': 'off','placeholder': 'ej: 56950123456',}),
        }
    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

class ProductoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['descripcion'].widget.attrs['autofocus'] = True

    def clean_field(self):
        data = self.cleaned_data['field']
        if not data:
            data = datetime.datetime.now()

        return data

    class Meta:
        model = Producto
        fields = '__all__'
        exclude = ('codigo',)
        widgets = {
            'descripcion': forms.Textarea(attrs={'class': 'form-control','autocomplete': 'off', 'rows': '3'}),
            'precio_compra': forms.NumberInput(attrs={'class': 'form-control','autocomplete': 'off'}),
            'precio_venta': forms.NumberInput(attrs={'class': 'form-control','autocomplete': 'off'}),
            'stock': forms.NumberInput(attrs={'class': 'form-control','autocomplete': 'off'}),
            'stock_critico': forms.NumberInput(attrs={'class': 'form-control','autocomplete': 'off'}),
            'fecha_vencimiento': forms.DateInput(
                format='%d/%m/%Y',
                attrs={
                    'class': 'form-control datetimepicker-input', 
                    'autocomplete': 'off',
                    'data-target': '#date_venc',
                    'data-toggle': 'datetimepicker'
                    }),
            'id_proveedor': forms.Select(attrs={'class': 'form-control select2', 'data-placeholder': '...'}),
            'id_familia': forms.Select(attrs={'class': 'form-control select2', 'data-placeholder': '...'}),
            'id_tipo_producto': forms.Select(attrs={'class': 'form-control select2', 'data-placeholder': '...'}),
            'image': forms.FileInput(attrs={'class': 'form-control','autocomplete': 'off'}),
        }
    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data
     
"""
select2" data-placeholder="..."
<!-- Date -->
                <div class="form-group">
                  <label>Date:</label>
                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate"/>
                        <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>

"""