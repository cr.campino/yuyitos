from django.urls import path
from django.contrib.auth.decorators import login_required
from . import views
app_name = 'almacen'

urlpatterns = [
    path('inicio/', views.DashboardView.as_view(), name='inicio'),
    path('productos/', views.ProductoListView.as_view(), name='producto_list'),
    path('productos/<str:pk>', views.ProductoDetailView.as_view(), name='producto_detail'),
    path('productos/create/', views.ProductoCreate.as_view(), name='producto_create'),
    path('productos/<str:pk>/update/', views.ProductoUpdate.as_view(), name='producto_update'),
    path('productos/<str:pk>/delete/', views.ProductoDelete.as_view(), name='producto_delete'),
    path('familia/', views.FamiliaListView.as_view(), name='familia_list'),
    path('familia/<str:pk>', views.FamiliaDetailView.as_view(), name='familia-detail'),
    path('familia/create/', views.FamiliaCreate.as_view(), name='familia_create'),
    path('familia/<str:pk>/update/', views.FamiliaUpdate.as_view(), name='familia_update'),
    path('familia/<str:pk>/delete/', views.FamiliaDelete.as_view(), name='familia_delete'),
    path('tipoventa/', views.TipoVListView.as_view(), name='tipov_list'),
    path('tipoventa/create/', views.TipoVCreate.as_view(), name='tipov_create'),
    path('tipoproducto/', views.TipoListView.as_view(), name='tipo_list'),
    path('tipoproducto/<str:pk>', views.TipoDetailView.as_view(), name='tipo_detail'),
    path('tipoproducto/create/', views.TipoCreate.as_view(), name='tipo_create'),
    path('tipoproducto/<str:pk>/update/', views.TipoUpdate.as_view(), name='tipo_update'),
    path('tipoproducto/<str:pk>/delete/', views.TipoDelete.as_view(), name='tipo_delete'),
    path('clientes/', views.ClienteListView.as_view(), name='cliente_list'),
    path('clientes/<str:pk>', views.ClienteDetailView.as_view(), name='cliente_detail'),
    path('clientes/create/', views.ClienteCreate.as_view(), name='cliente_create'),
    path('clientes/<str:pk>/update/', views.ClienteUpdate.as_view(), name='cliente_update'),
    path('clientes/<str:pk>/delete/', views.ClienteDelete.as_view(), name='cliente_delete'),
    path('credito/', views.CreditoListView.as_view(), name='credito'),
    path('proveedor/', views.ProveedorListView.as_view(), name='proveedor_list'),
    path('proveedor/<str:pk>', views.ProveedorDetailView.as_view(), name='proveedor_detail'),
    path('proveedor/create/', views.ProveedorCreate.as_view(), name='proveedor_create'),
    path('proveedor/<str:pk>/update/', views.ProveedorUpdate.as_view(), name='proveedor_update'),
    path('proveedor/<str:pk>/delete/', views.ProveedorDelete.as_view(), name='proveedor_delete'),
    path('compras/', views.OrdenListView.as_view(), name='compra_list'),
    path('compras/create/', views.OrdenCreate.as_view(), name='compra_create'),
    path('compras/update/<int:pk>/', views.OrdenUpdate.as_view(), name='orden_update'),
    path('compras/delete/<int:pk>/', views.OrdenDelete.as_view(), name='orden_delete'),
    path('recepcion/', views.RecepcionView.as_view(), name='recepcion'),
    # test
    path('test/', views.TestView.as_view(), name='test'),
    path('ventas/', views.VentaListView.as_view(), name='venta_list'),
    path('venta/create/', views.VentaCreate.as_view(), name='venta_create'),
    #path('productos/barcode/pdf/', views.BarCode.as_view(), name='barcode_pdf'),
    path('venta/invoice/pdf/<int:pk>/', views.VentaInvoicePdfView.as_view(), name='venta_invoice_pdf'),
    path('productos/reporte/pdf/', login_required(views.generar_reporte_productos), name='producto_reporte_pdf'),
    path('reporte/venta/', views.ReportVentaView.as_view(),name='venta_reporte'),
    path('reporte/compra/', views.ReportCompraView.as_view(),name='compra_reporte'),
]
