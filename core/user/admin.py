from django.contrib import admin
from core.user.models import Profile
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

ADDITIONAL_USER_FIELDS = (
    (None, {'fields': ('image','user_profile',)}),
)

class ProfileAdmin(UserAdmin):
    model = Profile

    add_fieldsets = UserAdmin.add_fieldsets + ADDITIONAL_USER_FIELDS
    fieldsets = UserAdmin.fieldsets + ADDITIONAL_USER_FIELDS

admin.site.register(Profile, ProfileAdmin)


admin.site.site_header = "Sitio web de inventarios"
admin.site.site_title = "Portal de inventarios"
admin.site.index_title = "Bienvenido al portal de administración"

