from django import forms
from django.forms import ModelForm, inlineformset_factory
from django.forms import SelectDateWidget
from .models import Profile
from django.contrib.auth.models import User
from django.contrib.auth.forms import (
    UserChangeForm,
    UserCreationForm
)

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(render_value=True), required=True)
    class Meta:
        model = Profile
        fields = '__all__'
        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control','autocomplete': 'off'}),
            'image': forms.FileInput(attrs={'class': 'form-control','autocomplete': 'off'}),
            'user_profile': forms.Select(attrs={'class': 'form-control select2', 'data-placeholder': '...'}),
            #'groups': forms.Select(attrs={'class': 'form-control select2', 'data-placeholder': '...'}),
        }

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['class'] = 'form-control'
        self.fields['password'].widget.attrs['class'] = 'form-control'

