from django.db import models
from yuyitos.settings import MEDIA_URL, STATIC_URL
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse 
from django.contrib.auth.models import AbstractUser
from django.forms import model_to_dict

class Profile(AbstractUser):
    ROLE_CHOICES = (
        ('vendedor', 'Vendedor'),
        ('operador', 'Operador'),
    )
    #user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='users/%Y/%m/%d', null=True, blank=True)
    user_profile = models.CharField(max_length=255, choices=ROLE_CHOICES,null=True, blank=True)
    def get_image(self):
        if self.image:
            return '{}{}'.format(MEDIA_URL, self.image)
        return '{}{}'.format(STATIC_URL, 'dist/img/empty.png')

    class Meta:
        db_table = 'auth_user'
