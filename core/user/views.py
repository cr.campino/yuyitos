from django.middleware.csrf import logger
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile
#from .forms import CreateProfileForm
#from .forms import CreateUserForm
from .forms import UserForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView, UpdateView, DeleteView, View
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView, FormView
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.template import RequestContext
from django.contrib.auth.models import Group
from django.http import JsonResponse

class Create(CreateView):
	success_url = reverse_lazy('user:usuarios')
	template_name = 'views/pages/usuario.html'
	model = Profile
	form_class = UserForm

	def form_valid(self,form):
		self.object = form.save(commit=False)
		self.object.set_password(self.object.password)
		self.object.save()	
		my_group = Group.objects.get(name='Operadores') 
		my_group.user_set.add(self.object)
		return HttpResponseRedirect(self.get_success_url())
	