$(function () {

    $('.select2').select2({
        theme: "bootstrap4",
        language: 'es',
        width: '100%'
    });

    $('#date_venc').datetimepicker({
        format: 'DD/MM/YYYY',
        locale: 'es',
        minDate: moment().format("YYYY-MM-DD")
    });
    
});