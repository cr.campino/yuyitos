var tblSale;
$(function () {
    tblSale = $('#data').DataTable({
        responsive: true,
        //scrollX: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        columns: [
            {"data": "id"},
            {"data": "id_tipo_venta.tipo_venta"},
            {"data": "id_cliente.nombre_cliente"},
            {"data": "fecha_hora"},
            {"data": "subtotal"},
            {"data": "total"},
            {"data": "total"},
        ],
        columnDefs: [
            {
                targets: [-2, -3],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return '$' + parseInt(data);
                }
            },
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    var buttons ='<a rel="details" class="btn btn-success btn-sm"><i class="fas fa-search" style="color:white"></i></a> ';
                    return buttons;
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });
    $('#data tbody')
    .on('click', 'a[rel="details"]', function () {
        var tr = tblSale.cell($(this).closest('td, li')).index();
        var data = tblSale.row(tr.row).data();
        console.log(data);

        $('#tblDet').DataTable({
            responsive: true,
            autoWidth: false,
            destroy: true,
            deferRender: true,
            //data: data.det,
            ajax: {
                url: window.location.pathname,
                type: 'POST',
                data: {
                    'action': 'search_details_prod',
                    'id': data.id
                },
                dataSrc: ""
            },
            columns: [
                {"data": "codigo.codigo"},
                {"data": "codigo.descripcion"},
                {"data": "codigo.id_familia.familia"},
                {"data": "precio"},
                {"data": "cantidad_venta_producto"},
                {"data": "subtotal"},
            ],
            columnDefs: [
                {
                    targets: [-1, -3],
                    class: 'text-center',
                    render: function (data, type, row) {
                        return '$' + parseInt(data);
                    }
                },
                {
                    targets: [-2],
                    class: 'text-center',
                    render: function (data, type, row) {
                        return data;
                    }
                },
            ],
            initComplete: function (settings, json) {

            }
        });

        $('#myModelDet').modal('show');
    })

});