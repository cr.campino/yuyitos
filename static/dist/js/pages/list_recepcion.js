var tblSale;
var tblD;
function message_error(obj) {
    var html = '';
    if (typeof (obj) === 'object') {
        html = '<ul style="text-align: left;">';
        $.each(obj, function (key, value) {
            html += '<li>' + key + ': ' + value + '</li>';
        });
        html += '</ul>';
    } else {
        html = '<p>' + obj + '</p>';
    }
    Swal.fire({
        title: 'Error!',
        html: html,
        icon: 'error'
    });
}
function submit_with_ajaxx(url, title, content, parameters, callback) {
    $.confirm({
        theme: 'material',
        title: title,
        icon: 'fa fa-info',
        content: content,
        columnClass: 'small',
        typeAnimated: true,
        cancelButtonClass: 'btn-primary',
        draggable: true,
        dragWindowBorder: false,
        buttons: {
            info: {
                text: "Si",
                btnClass: 'btn-primary',
                action: function () {
                    $.ajax({
                        url: url, //window.location.pathname
                        type: 'POST',
                        data: parameters,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                    }).done(function (data) {
                        console.log(data);
                        if (!data.hasOwnProperty('error')) {
                            callback(data);
                            return false;
                        }
                        message_error(data.error);
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        alert(textStatus + ': ' + errorThrown);
                    }).always(function (data) {

                    });
                }
            },
            danger: {
                text: "No",
                btnClass: 'btn-red',
                action: function () {

                }
            },
        }
    })
}

$(function () {
    $('.select2').select2({
        theme: "bootstrap4",
        language: 'es',
        width: '100%'
    });
    tblSale = $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdatacompra'
            },
            dataSrc: ""
        },
        columns: [
            {"data": "id"},
            {"data": "id_proveedor.nombre_proveedor"},
            {"data": "estado_orden"},
            {"data": "fecha_orden"},
            {"data": "total"},
            {"data": "total"},
        ],
        columnDefs: [
            {
                targets: [-2],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return '$' + parseInt(data);
                }
            },
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    var buttons ='<a rel="details" class="btn btn-success btn-sm" style="color:white"><i class="fas fa-search" ></i>&nbsp; Detalle</a> ';
                    buttons += '<a rel="recept" class="btn btn-warning btn-sm"><i class="ion ion-archive"></i>&nbsp; Recepcionar</a>';
                    return buttons;
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });
    $('#datafin').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'search_details_rec'
            },
            dataSrc: ""
        },
        columns: [
            {"data": "id"},
            {"data": "fecha_recepcion"},
            {"data": "orden"},
        ],
        columnDefs: [
            {
                targets: [-1,-2, -3],
                class: 'text-center',
            },

        ],
        initComplete: function (settings, json) {

        }
    });

    $('#data tbody').on('click', 'a[rel="recept"]', function (e){
        var tr = tblSale.cell($(this).closest('td, li')).index();
        var data = tblSale.row(tr.row).data();

        $('input[name="action"]').val('edit');
        $('input[name="id"]').val(data.id);
        tblD = $('#tblDeta').DataTable({
            responsive: true,
            autoWidth: false,
            destroy: true,
            deferRender: true,
            //data: data.det,
            ajax: {
                url: window.location.pathname,
                type: 'POST',
                data: {
                    'action': 'search_details_prod_compra',
                    'id': data.id
                },
                dataSrc: ""
            },
            columns: [
                {"data": "codigo.codigo"},
                {"data": "codigo.descripcion"},
                {"data": "codigo.id_familia.familia"},
                {"data": "precio"},
                {"data": "cantidad_orden_producto"},
                {"data": "subtotal"},
            ],
            columnDefs: [
                {
                    targets: [-1, -3],
                    class: 'text-center',
                    render: function (data, type, row) {
                        return '$' + parseInt(data);
                    }
                },
                {
                    targets: [-2],
                    class: 'text-center',
                    render: function (data, type, row) {
                        return data;
                    }
                },
            ],
            initComplete: function (settings, json) {

            }
        });
        
        $('#myModelDeta').modal('show');
    });

    $('form').on('submit', function (e) {
        e.preventDefault();
        var parameters = new FormData(this);
        parameters.append('action', $('input[name="action"]').val());
        submit_with_ajaxx(window.location.pathname, 'Notificación', '¿Estas seguro de realizar la siguiente acción?', parameters, function () {
            location.reload();
        });
        
    });

    $('#data tbody').on('click', 'a[rel="details"]', function () {
        var tr = tblSale.cell($(this).closest('td, li')).index();
        var data = tblSale.row(tr.row).data();
        

        $('#tblDet').DataTable({
            responsive: true,
            autoWidth: false,
            destroy: true,
            deferRender: true,
            //data: data.det,
            ajax: {
                url: window.location.pathname,
                type: 'POST',
                data: {
                    'action': 'search_details_prod_compra',
                    'id': data.id
                },
                dataSrc: ""
            },
            columns: [
                {"data": "codigo.codigo"},
                {"data": "codigo.descripcion"},
                {"data": "codigo.id_familia.familia"},
                {"data": "precio"},
                {"data": "cantidad_orden_producto"},
                {"data": "subtotal"},
            ],
            columnDefs: [
                {
                    targets: [-1, -3],
                    class: 'text-center',
                    render: function (data, type, row) {
                        return '$' + parseInt(data);
                    }
                },
                {
                    targets: [-2],
                    class: 'text-center',
                    render: function (data, type, row) {
                        return data;
                    }
                },
            ],
            initComplete: function (settings, json) {

            }
        });

        $('#myModelDet').modal('show');
    });

});



