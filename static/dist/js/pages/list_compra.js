var tblSale;
var tblCom;
$(function () {
    tblSale = $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdatacompra'
            },
            dataSrc: ""
        },
        columns: [
            {"data": "id"},
            {"data": "fecha_orden"},
            {"data": "id_proveedor.nombre_proveedor"},
            {"data": "estado_orden"},
            {"data": "total"},
            {"data": "total"},
        ],
        columnDefs: [
            {
                targets: [-2],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return '$' + parseInt(data);
                }
            },
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    var buttons ='<a rel="details" class="btn btn-success btn-sm" style="color:white"><i class="fas fa-search" ></i>&nbsp; Detalle</a> ';
                    if ( row.estado_orden === 'Pendiente') {
                        buttons += '<a class="btn btn-danger btn-sm" href="/almacen/compras/delete/' + row.id + '/"><i class="fas fa-trash"></i></a>';
                    }  

                    return buttons;
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });

    tblCom = $('#datafin').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdatacomprafin'
            },
            dataSrc: ""
        },
        columns: [
            {"data": "id"},
            {"data": "fecha_orden"},
            {"data": "id_proveedor.nombre_proveedor"},
            {"data": "estado_orden"},
            {"data": "total"},
            {"data": "total"},
        ],
        columnDefs: [
            {
                targets: [-2],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return '$' + parseInt(data);
                }
            },
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    var buttons ='<a rel="detailss" class="btn btn-success btn-sm" style="color:white"><i class="fas fa-search" ></i>&nbsp; Detalle</a> ';
                    return buttons;
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });

    $('#data tbody')
    .on('click', 'a[rel="details"]', function () {
        var tr = tblSale.cell($(this).closest('td, li')).index();
        var data = tblSale.row(tr.row).data();
        console.log(data);

        $('#tblDet').DataTable({
            responsive: true,
            autoWidth: false,
            destroy: true,
            deferRender: true,
            //data: data.det,
            ajax: {
                url: window.location.pathname,
                type: 'POST',
                data: {
                    'action': 'search_details_prod_compra',
                    'id': data.id
                },
                dataSrc: ""
            },
            columns: [
                {"data": "codigo.codigo"},
                {"data": "codigo.descripcion"},
                {"data": "codigo.id_familia.familia"},
                {"data": "precio"},
                {"data": "cantidad_orden_producto"},
                {"data": "subtotal"},
            ],
            columnDefs: [
                {
                    targets: [-1, -3],
                    class: 'text-center',
                    render: function (data, type, row) {
                        return '$' + parseInt(data);
                    }
                },
                {
                    targets: [-2],
                    class: 'text-center',
                    render: function (data, type, row) {
                        return data;
                    }
                },
            ],
            initComplete: function (settings, json) {

            }
        });

        $('#myModelDet').modal('show');
    })

    $('#datafin tbody')
    .on('click', 'a[rel="detailss"]', function () {
        var tr = tblCom.cell($(this).closest('td, li')).index();
        var data = tblCom.row(tr.row).data();
        console.log(data);

        $('#tblDet').DataTable({
            responsive: true,
            autoWidth: false,
            destroy: true,
            deferRender: true,
            //data: data.det,
            ajax: {
                url: window.location.pathname,
                type: 'POST',
                data: {
                    'action': 'search_details_prod_compra',
                    'id': data.id
                },
                dataSrc: ""
            },
            columns: [
                {"data": "codigo.codigo"},
                {"data": "codigo.descripcion"},
                {"data": "codigo.id_familia.familia"},
                {"data": "precio"},
                {"data": "cantidad_orden_producto"},
                {"data": "subtotal"},
            ],
            columnDefs: [
                {
                    targets: [-1, -3],
                    class: 'text-center',
                    render: function (data, type, row) {
                        return '$' + parseInt(data);
                    }
                },
                {
                    targets: [-2],
                    class: 'text-center',
                    render: function (data, type, row) {
                        return data;
                    }
                },
            ],
            initComplete: function (settings, json) {

            }
        });

        $('#myModelDet').modal('show');
    })

});