var tblCre;
var tblAbo;
$(function () {
    tblCre = $('#data').DataTable({
        responsive: true,
        //scrollX: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        
        columns: [
            {"data": "id"},
            {"data": "estado_credito"},
            {"data": "fecha_convenida"},
            {"data": "venta.id"},
            {"data": "monto_venta"},
            {"data": "monto_pendiente"},
            {"data": "id_cliente.nombre_cliente", "defaultContent":""},
            {"data": "estado_credito"},
        ],
        columnDefs: [
            {
                targets: [-3, -4],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return '$' + parseInt(data);
                }
            },
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    var buttons ='<a rel="details" class="btn btn-success btn-sm" style="color:white"><i class="fas fa-search" ></i>&nbsp; Abonos</a> ';
                    buttons += '<a class="btn btn-warning btn-sm" rel="add"><i class="ion-cash"></i> &nbsp;Abonar</a>';
                    return buttons;
                }
            },

        ],
        
        initComplete: function (settings, json) {


        }
    });
    tblAbo = $('#datafin').DataTable({
        responsive: true,
        //scrollX: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdatafinal'
            },
            dataSrc: ""
        },
        
        columns: [
            {"data": "id"},
            {"data": "estado_credito"},
            {"data": "fecha_convenida"},
            {"data": "venta.id"},
            {"data": "monto_venta"},
            {"data": "monto_pendiente"},
            {"data": "id_cliente.nombre_cliente", "defaultContent":""},
            {"data": "estado_credito"},
        ],
        columnDefs: [
            {
                targets: [-3, -4],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return '$' + parseInt(data);
                }
            },
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    var buttons ='<a rel="detailss" class="btn btn-success btn-sm" style="color:white"><i class="fas fa-search" ></i>&nbsp; Abonos</a> ';
                    return buttons;
                }
            },

        ],
        
        initComplete: function (settings, json) {


        }
    });

    $('#data tbody').on('click', 'a[rel="add"]', function () {
        var tr = tblCre.cell($(this).closest('td, li')).index();
        var data = tblCre.row(tr.row).data();
        console.log(data);
        $('input[name="action"]').val('add');
        $('form')[0].reset();
        $('input[name="id"]').val(data.id);
        $('#modalAbonar').modal('show');
    });

    

    $('#data tbody')
    .on('click', 'a[rel="details"]', function () {
        var tr = tblCre.cell($(this).closest('td, li')).index();
        var data = tblCre.row(tr.row).data();
        console.log(data);

        $('#tblDet').DataTable({
            responsive: true,
            autoWidth: false,
            destroy: true,
            deferRender: true,
            //data: data.det,
            ajax: {
                url: window.location.pathname,
                type: 'POST',
                data: {
                    'action': 'search_details_prod',
                    'id': data.id
                },
                dataSrc: ""
            },
            columns: [
                {"data": "id"},
                {"data": "abonado"},
                {"data": "fecha_abono"},
            ],
            columnDefs: [
                {
                    targets: [-2],
                    class: 'text-center',
                    render: function (data, type, row) {
                        return '$' + parseInt(data);
                    }
                },
            ],
            initComplete: function (settings, json) {

            }
        });

        $('#myModelDet').modal('show');
    })

    $('#datafin tbody')
    .on('click', 'a[rel="detailss"]', function () {
        var tr = tblAbo.cell($(this).closest('td, li')).index();
        var data = tblAbo.row(tr.row).data();

        $('#tblDet').DataTable({
            responsive: true,
            autoWidth: false,
            destroy: true,
            deferRender: true,
            //data: data.det,
            ajax: {
                url: window.location.pathname,
                type: 'POST',
                data: {
                    'action': 'search_details_prod',
                    'id': data.id
                },
                dataSrc: ""
            },
            columns: [
                {"data": "id"},
                {"data": "abonado"},
                {"data": "fecha_abono"},
            ],
            columnDefs: [
                {
                    targets: [-2],
                    class: 'text-center',
                    render: function (data, type, row) {
                        return '$' + parseInt(data);
                    }
                },
            ],
            initComplete: function (settings, json) {

            }
        });

        $('#myModelDet').modal('show');
    })
});
